from flask import Flask, render_template, jsonify, Markup, request
import json, requests, numpy

app = Flask(__name__)


name = ""
year = []
team = []
fgp = []
tpp = []
ftp = []
rpg = []
apg = []
spg = []
bpg = []
ppg = []

career_stats =[]
career_stats.append((year,team,fgp,tpp,ftp,rpg,apg,spg,bpg,ppg))

@app.route("/")
def home_page():
    name = " "
    del year[::]
    del team[::]
    del fgp[::]
    del tpp[::]
    del ftp[::]
    del rpg[::]
    del apg[::]
    del spg[::]
    del bpg[::]
    del ppg[::]
    title = "Player History Homepage"
    return render_template('index.html', title=title)

@app.route("/display")
def display():
    title = name
    return render_template('get-data.html', title=title, data=table)

@app.route("/get_stats", methods=['GET','POST'])
def get_stats():
    if request.method == 'POST':
        fname = request.form['first_name']
        lname = request.form['last_name']
        global name
        name = fname + " " + lname
        jsn = requests.get("https://wnba.loafing.dev/player?firstname="+fname+"&lastname="+lname)
        data = jsn.json()
        for x in data['years']:
            year.append(x)
            jsn_stats = requests.get("https://wnba.loafing.dev/player?firstname="+fname+"&lastname="+lname+"&year="+x)
            stats = jsn_stats.json()
            for y in stats['stats']:
                team.append(y['team'])
                a = float(y['FG%']*100)
                fgp.append('%.2f'%a)
                b = float(y['3P%']*100)
                tpp.append('%.2f'%b)
                c = float(y['FT%']*100)
                ftp.append('%.2f'%c)
                rpg.append(y['RPG'])
                apg.append(y['APG'])
                spg.append(y['SPG'])
                bpg.append(y['BPG'])
                ppg.append(y['PPG'])
            title = name
            global table
            table = numpy.transpose(career_stats)
        return render_template('display.html', title=title, data=table)
    return render_template('display.html', title=name, data=table )

@app.errorhandler(500)
def internal_error(error):
    title = "Player History Homepage"
    msg = "Player Not Found!"
    msg2 = "Please Enter Another Player's Name"
    return render_template('index.html', title=title, msg=msg, msg2=msg2)

@app.route("/fgp_chart")
def fgp_chart():
    labels = year
    values = fgp
    title = name + ": FG %"
    return render_template('chart.html',values=values, labels=labels, max=70, title=title)

@app.route("/tpp_chart")
def tpp_chart():
    labels = year
    values = tpp
    title = name + ": 3P %"
    return render_template('chart.html',values=values, labels=labels, max=60, title=title)

@app.route("/ftp_chart")
def ftp_chart():
    labels = year
    values = ftp
    title = name + ": FT %"
    return render_template('chart.html',values=values, labels=labels, max=100, title=title)

@app.route("/rpg_chart")
def rpg_chart():
    labels = year
    values = rpg
    title = name + ": RPG"
    return render_template('chart.html',values=values, labels=labels, max=20, title=title)

@app.route("/apg_chart")
def apg_chart():
    labels = year
    values = apg
    title = name + ": APG"
    return render_template('chart.html',values=values, labels=labels, max=15, title=title)

@app.route("/spg_chart")
def spg_chart():
    labels = year
    values = spg
    title = name + ": SPG"
    return render_template('chart.html',values=values, labels=labels, max=10, title=title)

@app.route("/bpg_chart")
def bpg_chart():
    labels = year
    values = bpg
    title = name + ": BPG"
    return render_template('chart.html',values=values, labels=labels, max=3, title=title)

@app.route("/ppg_chart")
def ppg_chart():
    labels = year
    values = ppg
    title = name + ": PPG"
    return render_template('chart.html',values=values, labels=labels, max=50, title=title)








if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001)
